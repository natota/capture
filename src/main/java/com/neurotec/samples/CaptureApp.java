package com.neurotec.samples;

import java.awt.BorderLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import com.neurotec.lang.NCore;
import com.neurotec.samples.util.LibraryManager;

public final class CaptureApp {
	
	public static String regNum = null;

	private static JFrame frame;

	// ===========================================================
	// Public static method
	// ===========================================================

	public static void main(String[] args) {
		
		//regNum = args[0];
		LibraryManager.initLibraryPath();

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				frame = new JFrame();
				frame.setTitle("Capture Fingerprint");
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.addWindowListener(new WindowAdapter() {
					@Override
					public void windowClosing(WindowEvent e) {
						NCore.shutdown();
					}
				});
				frame.add(new MainPanel(), BorderLayout.CENTER);
				frame.pack();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
			}
		});
	}

	// ===========================================================
	// Private constructor
	// ===========================================================

	private CaptureApp() {
	}

	public static final JFrame getMainFrame() {
		return frame;
	}
}

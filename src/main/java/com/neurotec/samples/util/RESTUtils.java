package com.neurotec.samples.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.neurotec.samples.BioDTO;

public class RESTUtils {

	public static String upload(BioDTO bioDTO) {
		try {

			Boolean returnV = false;
			 JSONObject jsonObject = new JSONObject(bioDTO);
			URL url = new URL(""+BioConst.REST_URL+"/bio/save");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setConnectTimeout(5000);
			conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
			conn.setRequestProperty("Accept", "application/json");


			OutputStream os = conn.getOutputStream();
			os.write(jsonObject.toString().getBytes());
			os.close();

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			String output;
			StringBuilder sb = new StringBuilder();
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				sb.append(output);
			}

			conn.disconnect();

			return sb.toString();

		} catch (MalformedURLException e) {

			System.out.println(e);

		} catch (IOException e) {

			System.out.println(e);

		}
		return null;

	}
	
	public static List<VolunteerFingerDtls> getALlFPTemplatesFromServer() {

		List<VolunteerFingerDtls> vdtls = new ArrayList<VolunteerFingerDtls>();
		try {
			String data = getJSON("" + BioConst.REST_URL + "/bio/templates", 500000000);
			if (data == null)
				return null;
			List<LinkedTreeMap<Object, Object>> array = new Gson().fromJson(data, List.class);
			for (int i = 0; i < array.size(); i++) {
				LinkedTreeMap<Object, Object> linkedTreeMap = array.get(i);
				VolunteerFingerDtls dtls = new VolunteerFingerDtls();
				dtls.setDbid((String) linkedTreeMap.get("dbid"));
				if (linkedTreeMap.get("template") != null) {
					byte[] backByte = org.apache.commons.codec.binary.Base64
							.decodeBase64((String) linkedTreeMap.get("template"));
					dtls.setTemplate(backByte);
					vdtls.add(dtls);
				}
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return vdtls;

	}

	public static String getRegNumBYDBId(String dbid) {
		try {
			String data = getJSON("" + BioConst.REST_URL + "/bio/regnum/" + dbid + "", 5000);
			return data;
		} catch (Exception e) {
			System.out.println(e);
		}
		return null;

	}
	
	public static String checkRegNum(String regNum) {
		String data = getJSON(""+BioConst.REST_URL+"/bio/check/"+regNum+"", 500000000);
		return data;
	}
	
	public static String getJSON(String url, int timeout) {
		HttpURLConnection c = null;
		try {
			URL u = new URL(url);
			c = (HttpURLConnection) u.openConnection();
			c.setRequestMethod("GET");
			c.setRequestProperty("Content-length", "0");
			c.setUseCaches(false);
			c.setAllowUserInteraction(false);
			c.setConnectTimeout(500000000);
			c.setReadTimeout(timeout);
			c.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
			c.setRequestProperty("Accept", "application/json");
			c.setDoOutput(true);
			c.setDoInput(true);
			c.connect();
			int status = c.getResponseCode();

			switch (status) {
			case 200:
			case 201:

				BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream()));
				StringBuilder sb = new StringBuilder();
				String line;
				while ((line = br.readLine()) != null) {
					sb.append(line + "\n");
				}
				br.close();
				return sb.toString();
			}

		} catch (MalformedURLException ex) {
			System.out.println(ex);
		} catch (IOException ex) {
			System.out.println(ex);
		} finally {
			if (c != null) {
				try {
					c.disconnect();
				} catch (Exception ex) {
					System.out.println(ex);
				}
			}
		}
		return null;
	}
}

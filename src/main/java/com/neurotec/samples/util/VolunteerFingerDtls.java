package com.neurotec.samples.util;

import java.util.List;

public class VolunteerFingerDtls {

	private Long id;

	private String dbid;

	private byte[] template;
	
	private String templateStr;
	
	private String base64;
	
	private List<byte[]> bytesL;
	

	public String getBase64() {
		return base64;
	}

	public void setBase64(String base64) {
		this.base64 = base64;
	}

	public String getTemplateStr() {
		return templateStr;
	}

	public void setTemplateStr(String templateStr) {
		this.templateStr = templateStr;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDbid() {
		return dbid;
	}

	public void setDbid(String dbid) {
		this.dbid = dbid;
	}

	public byte[] getTemplate() {
		return template;
	}

	public void setTemplate(byte[] template) {
		this.template = template;
	}
	
	
	
	
	

}
